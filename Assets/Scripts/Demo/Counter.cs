using UnityEngine;
using UnityEngine.UI;

namespace Demo
{

    public class Counter : MonoBehaviour
    {
        public Text text;
        public Button button;

        private int counter = 0;

        void Start()
        {
            button.onClick.AddListener(ButtonOnClick);
            counter = PlayerPrefs.GetInt("counter", 0);
        }

        void ButtonOnClick()
        {
            counter++;
            PlayerPrefs.SetInt("counter", counter);
        }

        void Update()
        {
            text.text = $"Counter {counter}";
        }
    }

}